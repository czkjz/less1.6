# Less1.6

## Описание задания 
Написать docker-compose.yml c условиями:
  > 1. 3 контейнера nginx1 nginx2 grafana (https://hub.docker.com/r/grafana/grafana)
  > 2. Nginx1 port 8001 на Grafana
  > 3. Nginx2 port 8002 на Grafana 
  > 4. Grafana не пробрасывать ничего наружу.
  Она не должна подниматься если вы её стопорите руками
  > 5. Nginx1 и Nginx2 в разных сетях
  > 6. 2 конфига nginx которые будут пробрасывать на Grafana 